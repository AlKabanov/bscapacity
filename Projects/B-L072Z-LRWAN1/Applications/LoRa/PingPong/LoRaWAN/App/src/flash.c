/**
  ******************************************************************************
  * @file    flash.c
  * @author  A.Kabanoff
  * @brief   write and read from flash memory
  ******************************************************************************
  */
/* Includes ------------------------------------------------------------------*/
#include "flash.h"
/** @addtogroup FLASH_Program
  * @{
  */

/* Private typedef -----------------------------------------------------------*/
/* Private define ------------------------------------------------------------*/
#define FLASH_USER_START_ADDR   (FLASH_BASE + FLASH_PAGE_SIZE * 512)             /* Start @ of user Flash area */
#define FLASH_USER_END_ADDR     (FLASH_USER_START_ADDR + FLASH_PAGE_SIZE * 1024)   /* End @ of user Flash area */

#define PAGE_SAMPLES 28  //defines how many samples per second
#define REC_BUF (((PAGE_SAMPLES+1)*3)+1)

/* Private macro -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/
uint32_t Address = FLASH_USER_START_ADDR, PAGEError = 0;
uint32_t flashSlot, flashSlotPtr;
__IO uint32_t data32 = 0 , MemoryProgramStatus = 0;

volatile uint32_t debug1 = 0,debug2= 0,debug3 = 0,debug4 = 0;

/*Variable used for Erase procedure*/
static FLASH_EraseInitTypeDef EraseInitStruct;

/* Private function prototypes -----------------------------------------------*/

/* Private functions ---------------------------------------------------------*/
  
/* Public functions ---------------------------------------------------------*/ 

/**
  * @brief calculate how many bytes should be in one message from necksensor
  * @param non
  * @retval number of byte to receive
  */
uint32_t flashBuffSize(void)
{
  return REC_BUF;
}

/**
  * @brief erase user flash memory
  * @param non
  * @retval true if ok, false if error
  */
bool flashInit(void)
{
  /* calculate flash slot*/
  flashSlot = REC_BUF%4?REC_BUF + 4 - REC_BUF%4:REC_BUF;
  /* Unlock the Flash to enable the flash control register access *************/
  HAL_FLASH_Unlock();

  /* Erase the user Flash area
    (area defined by FLASH_USER_START_ADDR and FLASH_USER_END_ADDR) ***********/

  /* Fill EraseInit structure*/
  EraseInitStruct.TypeErase   = FLASH_TYPEERASE_PAGES;
  EraseInitStruct.PageAddress = FLASH_USER_START_ADDR;
  EraseInitStruct.NbPages     = (FLASH_USER_END_ADDR - FLASH_USER_START_ADDR) / FLASH_PAGE_SIZE;

  HAL_StatusTypeDef erase = HAL_FLASHEx_Erase(&EraseInitStruct, &PAGEError);
  
  /* Lock the Flash to disable the flash control register access (recommended
     to protect the FLASH memory against possible unwanted operation) *********/
  HAL_FLASH_Lock();
  if (erase!= HAL_OK)return false;
      /*
      Error occurred while page erase.
      User can add here some code to deal with this error.
      PAGEError will contain the faulty page and then to know the code error on this page,
      user can call function 'HAL_FLASH_GetError()'
    */
  return true;
  
}

/**
  * @brief Program the user Flash area word by word
  * (area defined by FLASH_USER_START_ADDR and FLASH_USER_END_ADDR)
  * @param Buffer - data Array to store to flash
  * @param size - size of data Array
  * @retval true if ok, false if error
  */

bool flashWrite(uint8_t* Buffer, uint32_t size)
{
  uint8_t *progArrStart;
  uint32_t index = 0;
  bool noError = true;
  uint32_t endAddress = Address+size;
  if(endAddress > FLASH_USER_END_ADDR)return false;
  if(!(progArrStart = (uint8_t *)malloc(size)))return false;
  memcpy(progArrStart,Buffer,size);
  
  /* Unlock the Flash to enable the flash control register access *************/
  
  HAL_FLASH_Unlock();
  while (Address < endAddress)
  {
    if (HAL_FLASH_Program(FLASH_TYPEPROGRAM_WORD, Address, *(uint32_t*)(progArrStart+index)) == HAL_OK)
    {
      Address += 4;
      index +=4;
    }
    else noError = false;
  }
  free(progArrStart);
  /* Lock the Flash to disable the flash control register access (recommended
     to protect the FLASH memory against possible unwanted operation) *********/
  HAL_FLASH_Lock();
  return noError;
}
/**
  * @brief convert data in flash into strings for csv 
  *  "x","y","z"\n\r 
  *   1,2,3\n\r
  *   -1,-2,-3\n\r
  * @param NoN
  * @retval string to send to PC
  */
char firstStr[] = "\"X\",\"Y\",\"Z\",\"OVERRANGED\"\n\r";
char errorStr[] = "Error\n\r";
char str[40];
union {
  uint32_t words[REC_BUF/4 + 1];
  uint8_t rawData[3][PAGE_SAMPLES+1]; 
} flashData;
uint32_t flashDataPtr = 0;
uint32_t strCounter = 0;
int16_t X = 0, Y = 0, Z = 0;

bool stopString = false;


char* flashGetStr(void)
{
  if((stopString == false)&&(flashSlot))
  {
    strCounter++;
    if(strCounter == 1)  //preparation for flash reading
    {
      flashDataPtr = 0;
      flashSlotPtr = FLASH_USER_START_ADDR;
      return firstStr;
    }
    if(flashDataPtr == 0)//send base values
    {
       for(int i = 0;i < REC_BUF/4 + 1;i++)
        {flashData.words[i]  = *(__IO uint32_t *)(flashSlotPtr + i*4);}
          
      X = (((uint16_t)flashData.rawData[0][0]) << 8) + flashData.rawData[0][1];
      Y = (((uint16_t)flashData.rawData[1][0]) << 8) + flashData.rawData[1][1];
      Z = (((uint16_t)flashData.rawData[2][0]) << 8) + flashData.rawData[2][1];
      flashDataPtr +=2;
    }
    else
    {
      X += (int8_t)flashData.rawData[0][flashDataPtr];
      Y += (int8_t)flashData.rawData[1][flashDataPtr];
      Z += (int8_t)flashData.rawData[2][flashDataPtr];    
      if(flashDataPtr == PAGE_SAMPLES)
      {
        flashDataPtr = 0;
        flashSlotPtr += flashSlot;
      }
      else flashDataPtr++;
    }
    sprintf(str,"%d,%d,%d,%d\n\r",X,Y,Z,flashData.rawData[2][PAGE_SAMPLES+1]);
    if(flashSlotPtr >= Address)stopString = true;
    return str;
  }
  else return errorStr;
}
/* Check if the programmed data is OK
      MemoryProgramStatus = 0: data programmed correctly
      MemoryProgramStatus != 0: number of words not programmed correctly 
  Address = FLASH_USER_START_ADDR;
  MemoryProgramStatus = 0x0;

  while (Address < FLASH_USER_END_ADDR)
  {
    data32 = *(__IO uint32_t *)Address;

    if (data32 != DATA_32)
    {
      MemoryProgramStatus++;
    }
    Address = Address + 4;
  }
******/  
/**
  * @brief indicates end of data
  * @param NoN
  * @retval true - continue to send
  *         false - stop sending
  */
bool flashEOF(void)
{
  if(stopString == true)
  {
    stopString = false;
    strCounter = 0;
    return false;
  }
  else return true;
}
/**
  * @}
  */

/*****************************END OF FILE****/